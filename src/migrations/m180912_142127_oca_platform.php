<?php

use yii\db\Migration;

/**
 * Class m180912_142127_oca_platform
 */
class m180912_142127_oca_platform extends Migration
{
    public function init()
    {
        $this->db= 'db_tracking';
        parent::init(); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('tracking_platform', [
            'name' => 'Oca',
            'class_name' => 'quoma\tracking\platforms\oca\Oca',
            'status' => 'enabled'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('tracking_platform',['name' => 'Oca']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180912_142127_oca_platform cannot be reverted.\n";

        return false;
    }
    */
}
