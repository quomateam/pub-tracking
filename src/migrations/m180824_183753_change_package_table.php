<?php

use yii\db\Migration;

/**
 * Class m180824_183753_change_package_table
 */
class m180824_183753_change_package_table extends Migration
{

    public function init(){
        $this->db= 'db_tracking';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('package', 'customer_name', 'VARCHAR(45) NULL');
        $this->alterColumn('package', 'customer_lastname', 'VARCHAR(45) NULL');
        $this->alterColumn('package', 'street', 'VARCHAR(90) NULL');
        $this->alterColumn('package', 'floor', 'VARCHAR(45) NULL');
        $this->alterColumn('package', 'house', 'VARCHAR(45) NULL');
        $this->alterColumn('package', 'locality', 'VARCHAR(45) NULL');
        $this->alterColumn('package', 'province', 'VARCHAR(45) NULL');
        $this->alterColumn('package', 'cp', 'VARCHAR(45) NULL');

        $this->addColumn('package', 'number',' VARCHAR(45) NULL AFTER street');
        $this->addColumn('package', 'shipping_type', 'ENUM("door_to_door", "door_to_branch") NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('package', 'customer_name', 'VARCHAR(45) NOT NULL');
        $this->alterColumn('package', 'customer_lastname', 'VARCHAR(45) NOT NULL');
        $this->alterColumn('package', 'street', 'VARCHAR(90) NOT NULL');
        $this->alterColumn('package', 'floor', 'VARCHAR(45) NOT NULL');
        $this->alterColumn('package', 'house', 'VARCHAR(45) NOT NULL');
        $this->alterColumn('package', 'locality', 'VARCHAR(45) NOT NULL');
        $this->alterColumn('package', 'province', 'VARCHAR(45) NOT NULL');
        $this->alterColumn('package', 'cp', 'VARCHAR(45) NOT NULL');

        $this->dropColumn('package', 'number');
        $this->dropColumn('package', 'shipping_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180824_183753_change_package_table cannot be reverted.\n";

        return false;
    }
    */
}
