<?php

use yii\db\Migration;

/**
 * Class m180821_181305_create_database_and_tables
 */
class m180821_181305_create_database_and_tables extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_tracking'));

        $this->execute("DROP SCHEMA IF EXISTS `$db`;");
        $this->execute("CREATE SCHEMA IF NOT EXISTS `$db` DEFAULT CHARACTER SET utf8");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `$db`.`tracking_platform` (
                  `tracking_platform_id` INT NOT NULL AUTO_INCREMENT,
                  `name` VARCHAR(45) NOT NULL,
                  `class_name` VARCHAR(255) NOT NULL,
                  `status` ENUM('enabled', 'disabled') NOT NULL,
                  PRIMARY KEY (`tracking_platform_id`))
                ENGINE = InnoDB");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `$db`.`site` (
              `site_id` INT NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(45) NOT NULL,
              `status` ENUM('enabled', 'disabled') NOT NULL,
              `site_token` VARCHAR(255) NOT NULL,
              `tracking_platform_id` INT NOT NULL,
              PRIMARY KEY (`site_id`),
              INDEX `fk_site_tracking_platform_idx` (`tracking_platform_id` ASC),
              CONSTRAINT `fk_site_tracking_platform`
                FOREIGN KEY (`tracking_platform_id`)
                REFERENCES `tracking_platform` (`tracking_platform_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `$db`.`platform_config` (
                  `platform_config_id` INT NOT NULL AUTO_INCREMENT,
                  `attribute` VARCHAR(45) NOT NULL,
                  `value` VARCHAR(45) NULL,
                  `site_id` INT NOT NULL,
                  PRIMARY KEY (`platform_config_id`),
                  INDEX `fk_platform_config_site1_idx` (`site_id` ASC),
                  CONSTRAINT `fk_platform_config_site1`
                    FOREIGN KEY (`site_id`)
                    REFERENCES `site` (`site_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `$db`.`package` (
                  `package_id` INT NOT NULL AUTO_INCREMENT,
                  `status` ENUM('draft', 'confirm', 'pending', 'in_proccess', 'delivered') NOT NULL,
                  `created_at` INT NOT NULL,
                  `updated_at` INT NOT NULL,
                  `customer_lastname` VARCHAR(45) NOT NULL,
                  `customer_name` VARCHAR(45) NOT NULL,
                  `street` VARCHAR(90) NOT NULL,
                  `floor` VARCHAR(45) NOT NULL,
                  `house` VARCHAR(45) NOT NULL,
                  `locality` VARCHAR(45) NOT NULL,
                  `province` VARCHAR(45) NOT NULL,
                  `cp` VARCHAR(45) NOT NULL,
                  `phone` VARCHAR(45) NULL,
                  `email` VARCHAR(90) NULL,
                  `site_id` INT NOT NULL,
                  `external_id` VARCHAR(255) NULL,
                  PRIMARY KEY (`package_id`),
                  INDEX `fk_package_site1_idx` (`site_id` ASC),
                  CONSTRAINT `fk_package_site1`
                    FOREIGN KEY (`site_id`)
                    REFERENCES `site` (`site_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB");

        $this->execute(
            "CREATE TABLE IF NOT EXISTS `$db`.`package_has_product` (
              `package_has_product_id` INT NOT NULL AUTO_INCREMENT,
              `product_class` VARCHAR(255) NOT NULL,
              `product_id` INT NOT NULL,
              `package_id` INT NOT NULL,
              PRIMARY KEY (`package_has_product_id`),
              INDEX `fk_package_has_product_package1_idx` (`package_id` ASC),
              CONSTRAINT `fk_package_has_product_package1`
                FOREIGN KEY (`package_id`)
                REFERENCES `package` (`package_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB");

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = quoma\products\components\DbHelper::getDbName(Yii::$app->get('db_tracking'));

        $this->execute("DROP SCHEMA IF EXISTS $db;");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180821_181305_create_database_and_tables cannot be reverted.\n";

        return false;
    }
    */
}
