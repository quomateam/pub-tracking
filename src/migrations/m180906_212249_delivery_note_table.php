<?php

use yii\db\Migration;

/**
 * Class m180906_212249_delivery_note_table
 */
class m180906_212249_delivery_note_table extends Migration
{
    public function init(){
        $this->db='db_tracking';
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(
            "CREATE TABLE IF NOT EXISTS `delivery_note` (
                  `delivery_note_id` INT NOT NULL AUTO_INCREMENT,
                  `number` VARCHAR(255) NOT NULL,
                  `tracking_platform_id` INT NOT NULL,
                  `package_id` INT NOT NULL,
                  PRIMARY KEY (`delivery_note_id`),
                  INDEX `fk_delivery_note_tracking_platform1_idx` (`tracking_platform_id` ASC),
                  INDEX `fk_delivery_note_package1_idx` (`package_id` ASC),
                  CONSTRAINT `fk_delivery_note_tracking_platform1`
                    FOREIGN KEY (`tracking_platform_id`)
                    REFERENCES `tracking_platform` (`tracking_platform_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION,
                  CONSTRAINT `fk_delivery_note_package1`
                    FOREIGN KEY (`package_id`)
                    REFERENCES `package` (`package_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('delivery_note');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_212249_delivery_note_table cannot be reverted.\n";

        return false;
    }
    */
}
