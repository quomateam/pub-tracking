<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 22/08/18
 * Time: 12:11
 */

namespace quoma\tracking\components;


interface PlatformInterface
{

    /**
     * Debe realizar el pedido de retiro/envio del paquete
     * @param $package
     * @return mixed
     */
    public function send($package);

    /**
     * Debe devolver la cotizacion del paquete
     * @param $package
     * @return mixed
     */
    public function quote($package);

    /**
     * Debe cancelar el pedido de retiro/envio
     * @param $package
     * @return mixed
     */
    public function cancel($package);

    /**
     * Devuelve un array con los parametros necesarios a configurar para utilizar la plataforma.
     * El array debe tener la siguiente forma
     * [
     *     [
     *       label => 'Label a mostrar'
     *       attribute => 'nombre_attributo'
     *       type => 'text o check'
     *     ]
     * ]
     * @return mixed
     */
    public function getConfigParams();

    /**
     * Debe devolver la/s etiqueta para el package recibido
     * @param $package
     * @return mixed
     */
    public function getTags($package);

    public function updateStatus($package);
}