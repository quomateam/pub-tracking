<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 22/08/18
 * Time: 10:58
 */

namespace quoma\tracking\components;


use quoma\tracking\TrackingModule;

class Controller extends \quoma\core\web\Controller
{
    public function behaviors()
    {
        $behaviors= parent::behaviors();

        if (TrackingModule::getInstance()->use_user_module == false){
            $behaviors = [];
        }

        $behaviors= array_merge($behaviors, TrackingModule::getInstance()->extraBehaviors);

        return  $behaviors;
    }

}