<?php
use quoma\tracking\models\Package;
use quoma\tracking\TrackingModule;
?>

<div class="address-info">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                Datos de Envío
            </h3>
        </div>
        <div class="panel-body">
             <div class="row">
                 <div class="col-xs-12 col-md-6 address-info__first-block">
                    <h5>
                        Persona que recibe
                    </h5>
                    <h4>
                        <?php echo $package->customer_name . ' '. $package->customer_lastname?>
                    </h4>
                    <hr class="visible-xs-block">
                 </div>
                <?php if ($package->shipping_type === Package:: DOOR_TO_DOOR_SHIPPING_TYPE):?>
                     <div class="col-xs-12 col-md-6 address-info__second-block">
                        <h5>Domicilio de Entrega</h5>
                        <ul>
                            <li>
                                <?php echo Yii::t('app', 'Street')?>: <?php echo $package->street?>
                            </li>
                            <li>
                                <?php echo Yii::t('app','Number')?>: <?php echo $package->number?>
                            </li>
                            <li>
                                <?php echo Yii::t('app','Floor')?>: <?php echo $package->floor?>
                            </li>
                            <li>
                                <?php echo Yii::t('app','House')?>: <?php echo $package->house?>
                            </li>
                            <li>
                                <?php echo Yii::t('app','Locality')?>: <?php echo $package->locality?>
                            </li>
                            <li>
                                <?php echo Yii::t('app','Province')?>: <?php echo $package->province?>
                            </li>
                        </ul>
                     </div>
                <?php elseif($package->shipping_type === Package::DOOR_TO_BRANCH_SHIPPING_TYPE):?>
                     <div class="col-xs-12 col-md-6 address-info__second-block">

                            <h5>
                                Enviar a sucursal cercana de Oca
                            </h5>
                            <h4> <?php echo $package->street?></h4>

                     </div>
                <?php else: ?>
                    <div class="col-xs-12 col-md-6 address-info__second-block">

                        <h5>
                            Retirar en Local de Ventas
                        </h5>
                        <h4> <?php echo $package->localSales?></h4>

                    </div>
                <?php endif;?>
             </div>
        </div>
    </div>

</div>
