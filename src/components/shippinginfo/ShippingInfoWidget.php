<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 20/09/18
 * Time: 13:13
 */

namespace quoma\tracking\components\shippinginfo;


use quoma\tracking\models\Package;
use yii\bootstrap\Widget;

class ShippingInfoWidget extends Widget
{

    public $model_class;
    public $model_id;

    public function run()
    {
        $package= $this->getPackage();

        return $this->render('info', ['package' => $package]);
    }

    private function getPackage(){
        $package= Package::findOne(['model_class' => $this->model_class, 'model_id' => $this->model_id]);


        return $package;
    }
}