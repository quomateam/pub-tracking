<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 27/08/18
 * Time: 16:31
 */

namespace quoma\tracking\components;


use quoma\tracking\models\Package;
use quoma\tracking\models\Site;
use yii\base\Behavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class ShippingBehavior extends Behavior
{
    public $modelClass;
    public $is_cart;
    public $productRelationship = 'products';
    public $shippingSiteToken;

    public function events(){
        $events= [
            ActiveRecord::EVENT_AFTER_INSERT => 'createPackage',
            ActiveRecord::EVENT_AFTER_FIND => 'updatePackage',
         ];

        return $events;
    }

    public function createPackage(){
        $site= Site::getSite($this->shippingSiteToken);

        if (empty($site)){
            throw new BadRequestHttpException('Site Not Found');
        }

        if ($this->modelClass){
            $model= new Package([
                'model_class' => $this->modelClass,
                'model_id' => $this->owner->getPrimaryKey(),
                'status' => 'draft',
                'site_id' => $site->site_id,
                'shipping_type' => Package::LOCAL_SALES_SHIPPING_TYPE // Al crear el paquete lo seteamos para retirar en local,
                // si se cotiza el tipo cambiara de acuerdo a lo seleccionado
            ]);

        }else{
            $model= new Package([
                'model_class' => $this->owner->className(),
                'model_id' => $this->owner->getPrimaryKey(),
                'status' => 'draft',
                'site_id' => $site->site_id,
                'shipping_type' => Package::LOCAL_SALES_SHIPPING_TYPE // Al crear el paquete lo seteamos para retirar en local,
                // si se cotiza el tipo cambiara de acuerdo a lo seleccionado
            ]);
        }

        $model->save();

        \Yii::$app->session->set('package_id', $model->package_id);

        return $model;
    }

    public function getShippingPackage(){

        if (!empty(\Yii::$app->session->get('package_id', null))) {
            return Package::findOne(\Yii::$app->session->get('package_id'));
        }

        return Package::findOne(['model_class' => $this->modelClass, 'model_id' => $this->owner->getPrimaryKey()]);
    }

    public function updatePackage(){
        $package= $this->getShippingPackage();

        if (empty($package)){
            $package= $this->createPackage();
        }

        if ($this->is_cart ){
            if ($this->owner->hasProperty($this->productRelationship)){
                $method= $this->productRelationship;
                $old_products= $package->getProducts();
                $new_products= $this->owner->$method;

                //echo print_r($products,1); die();
                foreach ($new_products as $product){
                    if (!$package->hasProduct($product)){
                        $package->addProduct($product);
                    }
                }
            }
        }else{
            if (!$package->hasProduct($this->owner)){
                $package->addProduct($this->owner);
            }
        }
    }


    public function sendPackage(){
        $package= $this->getShippingPackage();

        if ($package->shipping_type !== Package::LOCAL_SALES_SHIPPING_TYPE){

            $platform= $package->site->getPlatform();

            $platform->send($package);
        }

    }

    public function getShippingPrice(){
        $package= $this->getShippingPackage();

        if (empty($package) || empty($package->price)){
            return 0;
        }

        return $package->price;
    }

    public function setPackagePending(){
        $package = $this->getShippingPackage();

        if (empty($package)){
            throw new NotFoundHttpException('Package Not found');
        }

        $package->updateAttributes(['status' => 'pending']);

        return true;

    }

    public function setProductQty($class, $model_id, $qty){
        $package= $this->getShippingPackage();

        if ($package){
            $product= $package->getPackageHasProducts()->where(['product_class' => $class, 'product_id' => $model_id])->one();

            if (empty($product)){
                $p= $class::findOne($model_id);
                $package->addProduct($p, $qty);
                return true;
            }

            $product->updateAttributes(['qty' => $qty]);
            return true;
        }

        return false;

    }

}