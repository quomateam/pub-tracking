<?php

namespace quoma\tracking;
use quoma\core\menu\Menu;
use quoma\core\module\QuomaModule;

/**
 * tracking module definition class
 */
class TrackingModule extends QuomaModule
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'quoma\tracking\controllers';

    public $multisite= false;

    public $site_required= false;

    public $use_user_module= true;

    public $extraBehaviors= [];

    public $branch_config= [];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->registerTranslations();

    }

    /**
     * @return Menu
     */
    public function getMenu(Menu $menu)
    {
        // TODO: Implement getMenu() method.
    }

    /**
     * Retorna un arreglo con los nombres de los modulos de los que se depende.
     *
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

}
