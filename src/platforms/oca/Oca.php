<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 22/08/18
 * Time: 12:10
 */

namespace quoma\tracking\platforms\oca;


use quoma\tracking\components\PlatformInterface;
use quoma\tracking\models\Package;
use quoma\tracking\models\PackageStatus;
use quoma\tracking\models\PlatformConfig;
use quoma\tracking\models\Site;
use yii\httpclient\Client;

class Oca implements PlatformInterface
{

    private $api_url= 'http://webservice.oca.com.ar/epak_tracking/Oep_TrackEPak.asmx';

    /**
     * Debe realizar el pedido de retiro/envio del paquete
     * @param $package
     * @return mixed
     */
    public function send($package)
    {
        //Si ya fue enviado, no se vuelve a enviar
        if ($package->external_id !== null){
            $package->updateAttributes(['status' => 'confirm']);
            return false;
        }
        $response= $this->IngresoORMultiple($package);

        if ($response !== false){
            $package->updateAttributes(['status' => 'confirm', 'external_id' => $response[0]->OrdenRetiro]);

            return true;
        }

        return false;
    }

    /**
     * Debe realizar la cotizacion del paquete
     * @param $package
     * @return mixed
     */
    public function quote($package)
    {
        if ($package->shipping_type !== Package::LOCAL_SALES_SHIPPING_TYPE){

            $weight= $package->getTotalWeight();
            $volume= $package->getTotalVolumen();

            if ($package->shipping_type === Package::DOOR_TO_DOOR_SHIPPING_TYPE){
                $operativa= PlatformConfig::getValue('oca_operativa_puerta_puerta', $package->site_id);
            }else{
                $operativa= PlatformConfig::getValue('oca_operativa_puerta_sucursal', $package->site_id);
            }

            $data= $this->tarifarEnvioCorporativo($weight,$volume,$package->cp, $package->getPiecesCount(), $package->site_id, $operativa);

            //Si el cotizador dio error, devolvemos falso para indicar el error
            if ($data  === false){
                return false;
            }

            $package->price= $data[0]->Total;

            $delivery= strtotime(date('Y-m-d')) + (86400 * $data[0]->PlazoEntrega);

            if (date('w', $delivery) == 6){
                $package->delivery_timestamp = $delivery + (86400 * 2);
            }elseif (date('w', $delivery) == 0){
                $package->delivery_timestamp = $delivery + 86400;
            }else{
                $package->delivery_timestamp= $delivery;
            }

            $package->updateAttributes(['price', 'delivery_timestamp', 'shipping_type', 'external_branch_id']);
        }else{
            $package->price= 0;
            $package->delivery_timestamp= null;

            $package->updateAttributes(['price', 'delivery_timestamp', 'shipping_type','external_branch_id']);
        }

        $response= [
            'price' => \Yii::$app->formatter->asCurrency((float)$package->price),
            'delivery' => $package->delivery_timestamp ? \Yii::$app->formatter->asDate($package->delivery_timestamp, 'dd-MM-yyyy')
                : PlatformConfig::getValue('delivery_time', $package->site_id),
        ];

        return $response;
    }

    /**
     * Debe cancelar el pedido de retiro/envio
     * @param $package
     * @return mixed
     */
    public function cancel($package)
    {
        $response= $this->anularOrdenGenerada($package);

        if ($response){
            $package->updateAttributes(['status' => 'canceled']);
        }

        return $response;
    }

    /**
     * Devuelve un array con los parametros necesarios a configurar para utilizar la plataforma.
     * El array debe tener la siguiente forma
     * [
     *     [
     *       label => 'Label a mostrar'
     *       attribute => 'nombre_attributo'
     *       type => 'text o check'
     *     ]
     * ]
     * @return mixed
     */
    public function getConfigParams()
    {
        return [
            [
                'label' => 'Usuario de Oca',
                'attribute' => 'oca_user',
                'type' => 'text'
            ],
            [
                'label' => 'Contraseña de Oca',
                'attribute' => 'oca_pass',
                'type' => 'text'
            ],
            [
                'label' => 'Número de Operativa Puerta a Puerta',
                'attribute' => 'oca_operativa_puerta_puerta',
                'type' => 'text'
            ],
            [
                'label' => 'Número de Operativa Puerta a Sucursal',
                'attribute' => 'oca_operativa_puerta_sucursal',
                'type' => 'text'
            ],
            [
                'label' => 'SAP',
                'attribute' => 'oca_sap',
                'type' => 'text'
            ],
            [
                'label' => 'Franja Horaria para retiros',
                'attribute' => 'oca_horario',
                'type' => 'drop',
                'options' => [
                    '1' => 'De 8hs a 17hs',
                    '2' => 'De 8hs a 12hs',
                    '3' => 'De 14hs a 17hs'
                ]
            ],
            [
                'label' => 'Nombre del Responsable',
                'attribute' => 'oca_resp_name',
                'type' => 'text'
            ],
            [
                'label' => 'Email del Responsable',
                'attribute' => 'oca_resp_email',
                'type' => 'text'
            ],
            [
                'label' => 'CUIT (con guiones)',
                'attribute' => 'oca_cuit',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - Calle',
                'attribute' => 'oca_dom_calle',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - Número',
                'attribute' => 'oca_dom_nro',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - Piso',
                'attribute' => 'oca_dom_piso',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - Departamento',
                'attribute' => 'oca_dom_dpto',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - CP',
                'attribute' => 'oca_dom_cp',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - Localidad',
                'attribute' => 'oca_dom_loc',
                'type' => 'text'
            ],
            [
                'label' => 'Domicilio del Local - Provincia',
                'attribute' => 'oca_dom_prov',
                'type' => 'text'
            ],
            [
                'label' =>  'Texto Tiempo de Entrega para retiros en local de ventas',
                'attribute' => 'delivery_time',
                'type' => 'text'
            ]

        ];
    }

    /**
     * Devuelve el listado de Provincias
     * @return \SimpleXMLElement
     */
    public  function getProvincias(){
        $data= new \SimpleXMLElement($this->api_url.'/GetProvincias', 0, true);

        return $data;
    }

    public function getSucursalesPorProvincia($idProvincia){
        $data= new \SimpleXMLElement($this->api_url.'/GetSucursalByProvincia?idProvincia='.$idProvincia, 0, true);

        return $data;
    }

    /**
     * Realiza la cotización contra la api de oca. Devuelve un objeto con la respuesta del service o false en caso de error
     * @param $weight // Peso Total del envio
     * @param $volume // Volumen Total del Envio
     * @param $origin_cp // Codigo Postal de origen
     * @param $destiny_cp // Codigo Postal de destino
     * @param $count // Cantidad de paquetes
     * @param $site_id // Id del sitio en el modulo Tracking
     * @param $operativa // Nro de operativa
     * @return bool
     */
    private function tarifarEnvioCorporativo($weight, $volume, $destiny_cp, $count, $site_id, $operativa){
        $data= [
            'PesoTotal' => $weight,
            'VolumenTotal' => number_format($volume, 8),
            'CodigoPostalOrigen' => PlatformConfig::getValue('oca_dom_cp', $site_id),
            'CodigoPostalDestino' => $destiny_cp,
            'CantidadPaquetes' => $count,
            'Cuit' => PlatformConfig::getValue('oca_cuit', $site_id),
            'Operativa' => $operativa,
            'ValorDeclarado' => 0
        ];

        \Yii::info('Info pasada '. print_r($data, 1), 'OCA');

        $ch= curl_init($this->api_url.'/Tarifar_Envio_Corporativo');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            \Yii::info('Falló curl Tarifar Envio: '. curl_getinfo($ch, CURLINFO_RESPONSE_CODE), 'OCA');
            \Yii::info('Falló curl Tarifar Envio: '. $response, 'OCA');
            curl_close($ch);
            return false;
        }
        \Yii::info('Respuesta satisfactoria Tarifar: '. $response, 'OCA');
        $response= str_replace('#Oca_e_Pak', '', $response);

        $data= simplexml_load_string($response, 'SimpleXMLElement' /**LIBXML_NOWARNING**/);

        $element= $data->xpath('//Table');

        return $element;
    }

    /**
     * Crea la estructura XML del envio
     * @param $package //Paquete a enviar
     * @param $operativa // Nro de operativa
     * @param $centroCosto // Espera la respuesta del metodo getCentroCostoPorOperativa
     * @param $idci // id de la sucursal de destino, solo en caso de que la operativa fuera puerta - sucursal
     * @return string
     */
    private function createXml($package, $operativa, $centroCosto, $idci){

        $xml= '<?xml version="1.0" encoding="iso-8859-1" standalone="yes"?>
                <ROWS>
                 <cabecera ver="2.0" nrocuenta="'.PlatformConfig::getValue('oca_sap', $package->site_id).'" />
                 <origenes>
                 <origen calle="'.PlatformConfig::getValue('oca_dom_calle', $package->site_id).'" nro="'
                    .PlatformConfig::getValue('oca_dom_nro', $package->site_id).'" piso="'
                    .PlatformConfig::getValue('oca_dom_piso', $package->site_id).'" depto="'
                    .PlatformConfig::getValue('oca_dom_dpto', $package->site_id).'" cp="'
                    .PlatformConfig::getValue('oca_dom_cp', $package->site_id).'"
                 localidad="'.PlatformConfig::getValue('oca_dom_loc', $package->site_id).'" provincia="'
                    .PlatformConfig::getValue('oca_dom_prov', $package->site_id).'" contacto="'. PlatformConfig::getValue('oca_resp_name', $package->site_id).'"
                 email="'.PlatformConfig::getValue('oca_resp_email', $package->site_id).'" solicitante="'
                    .$centroCosto->Solicitante.'" observaciones="" centrocosto="'. $centroCosto->NroCentroCosto. '" 
                 idfranjahoraria="'.PlatformConfig::getValue('oca_horario', $package->site_id).'" fecha="'.str_replace('-', '', date('Y-m-d')).'"
                 idcentroimposicionorigen="0">
                 <envios>
                 <envio idoperativa="'.$operativa.'" nroremito="'.$package->package_id.'">
                 <destinatario apellido="'.$package->customer_lastname.'" nombre="'.$package->customer_name.'" calle="'.$package->street.'" nro="'.$package->number.'"
                 piso="'.$package->floor.'" depto="'.$package->house.'" localidad="'.$package->locality.'" provincia="'.$package->province.'"
                 cp="'.$package->cp.'" telefono="'.$package->phone.'" email="'.$package->email.'" idci="'.$idci.'"
                 celular="'.$package->phone.'" />
                 <paquetes>';

        foreach ($package->packageHasProducts as $hasProduct){
            $product= $hasProduct->product;
            for ($i= 0; $i < $hasProduct->qty; $i++){
                $xml .= '<paquete alto="'.($product->height/10) .'" ancho="'.($product->width/10) .'" largo="'.($product->large/10) .'" peso="'.($product->weight/1000).'" valor="1" cant="1" />';
            }
        }

        $xml .='</paquetes></envio> </envios> </origen></origenes></ROWS>';

        return $xml;
    }

    /**
     * Genera la orden de retiro del paquete en Oca
     * @param $package
     * @return bool|\SimpleXMLElement[]
     */
    private function IngresoORMultiple($package){

        if ($package->shipping_type === Package::DOOR_TO_DOOR_SHIPPING_TYPE){
            $operativa= PlatformConfig::getValue('oca_operativa_puerta_puerta', $package->site_id);
            $idci= '0';
        }else{
            $operativa= PlatformConfig::getValue('oca_operativa_puerta_sucursal', $package->site_id);
            $idci= $package->branch_external_id;
        }

        $centroCosto= $this->getCentroCostoPorOperativa($operativa, $package->site_id);

        $data= [
            'usr' => PlatformConfig::getValue('oca_user', $package->site_id),
            'psw' => PlatformConfig::getValue('oca_pass', $package->site_id),
            'XML_Datos' => $this->createXml($package, $operativa, $centroCosto, $idci),
            //'DiasRetiro' => '1',
            'FranjaHoraria' => PlatformConfig::getValue('oca_horario', $package->site_id),
            'ConfirmarRetiro' => "true",
            'ArchivoCliente' => '',
            'ArchivoProceso' => ''
        ];

        //echo print_r($data, 1);

        \Yii::info('Info pasada '. print_r($data, 1), 'OCA');

        $ch= curl_init('http://webservice.oca.com.ar/ePak_tracking/Oep_TrackEPak.asmx/IngresoORMultiplesRetiros');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            \Yii::info('Falló curl Crear Ingreso: '. $response, 'OCA');
            curl_close($ch);
            return false;
        }
        \Yii::info('Respuesta satisfactoria Ingreso: '. $response);
        $response= str_replace('#Oca_e_Pak', '', $response);

        $data= simplexml_load_string($response, 'SimpleXMLElement' /**LIBXML_NOWARNING**/);

        $resumen= $data->xpath('//DetalleIngresos');

        return $resumen;
    }

    /**
     * Devuelve los Centro de Costo del cliente de acuerdo a la operativa
     * @param $operativa
     * @param $site_id
     * @return bool|\SimpleXMLElement
     */
    private function getCentroCostoPorOperativa($operativa, $site_id){
        $data= [
            'CUIT' => PlatformConfig::getValue('oca_cuit', $site_id),
            'Operativa' => $operativa
        ];


        \Yii::info('Info pasada '. print_r($data, 1), 'OCA');

        $ch= curl_init('http://webservice.oca.com.ar/oep_tracking/Oep_Track.asmx/GetCentroCostoPorOperativa');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/x-www-form-urlencoded'
        ]);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            \Yii::info('Falló curl Centro Operativa: '. $response, 'OCA');
            curl_close($ch);
            return false;
        }
        \Yii::info('Respuesta satisfactoria Centro Operativa: '. $response);
        $response= str_replace('#Oca_Express_Pak', '', $response);

        $data= simplexml_load_string($response, 'SimpleXMLElement' /**LIBXML_NOWARNING**/);

        $element= $data->xpath('//Table');
        return $element[0];

    }

    /**
     * Devuelve los centros de imposicion del pais (sucursales)
     * @return array
     */
    public function getCentrosImposicion(){
        $ch= curl_init('http://webservice.oca.com.ar/ePak_tracking/Oep_TrackEPak.asmx/GetCentrosImposicion');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Length: 0'
        ]);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            \Yii::info('Falló curl Centro Imposicion: '. curl_getinfo($ch, CURLINFO_RESPONSE_CODE), 'OCA');
            \Yii::info('Falló curl Centro Imposicion: '. $response, 'OCA');
            curl_close($ch);
            return [];
        }
        \Yii::info('Respuesta satisfactoria Centro Imposicion: '. $response);
        $response= str_replace('#Oca_e_Pak', '', $response);

        \Yii::info($response, 'Oca');

        $data= simplexml_load_string($response, 'SimpleXMLElement' /**LIBXML_NOWARNING**/);

        $centrosImposicion= $data->xpath('//Table');
        $result= [];


        for ($i= 0; $i < count($centrosImposicion); $i++) {
            $id= $centrosImposicion[$i]->idCentroImposicion;
            $cp= $centrosImposicion[$i]->CodigoPostal;
            $key= $id . '-'. $cp;
            $result[(string)$key]= $centrosImposicion[$i]->Descripcion . ' - '
                . $centrosImposicion[$i]->Calle . ' '
                . $centrosImposicion[$i]->Numero. ', '
                . $centrosImposicion[$i]->Localidad . ' - '
                . $centrosImposicion[$i]->Descripcion1;
        }

        return $result;
    }


    /**
     * Debe devolver la/s etiqueta para el package recibido
     * @param $package
     * @return mixed
     */
    public function getTags($package)
    {
        $data= [
            'idOrdenRetiro' => $package->external_id,
            'LogisticaInversa' => "true",
            'nroEnvio' => '',
        ];

        \Yii::info($data, 'OCA');

        $ch= curl_init("http://webservice.oca.com.ar/oep_tracking/Oep_Track.asmx/GetPdfDeEtiquetasPorOrdenOrNumeroEnvio");

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            \Yii::info($response, 'OCA');
            return false;
        }

        return base64_decode($response);

    }

    private function anularOrdenGenerada($package){

        $data= [
            'usr' => PlatformConfig::getValue('oca_user', $package->site_id),
            'psw' => PlatformConfig::getValue('oca_pass', $package->site_id),
            'idOrdenRetiro' => $package->external_id
        ];

        \Yii::info($data, 'OCA');

        $ch= curl_init("http://webservice.oca.com.ar/ePak_tracking/Oep_TrackEPak.asmx/AnularOrdenGenerada");

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            \Yii::info('Error Anular Orden: ' . $response);
            return false;
        }

        \Yii::info('Respuesta satisfactoria Anular Orden: ' . $response);

        return true;
    }


    public function updateStatus($package)
    {
        echo 'Updating status of package: ' . $package->external_id ."\n";
        $data= [
            'ordenRetiro' => $package->external_id,
            'numeroEnvio' => $package->external_id,
        ];


        $ch= curl_init("http://webservice.oca.com.ar/ePak_tracking/Oep_TrackEPak.asmx/GetEnvioEstadoActual");

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response= curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) !== 200){
            echo 'Error Status: ' . $response ."\n";
            \Yii::info('Error Status: ' . $response);
            return false;
        }
        echo "Communication succesfull.\n";

        $response= str_replace('#Oca_e_Pak', '', $response);
        $data= simplexml_load_string($response, 'SimpleXMLElement' /**LIBXML_NOWARNING**/);

        $statusses= $data->xpath('//Table');

        if (count($statusses) == 0) {
            echo "Nothing to do.\n";
        }
        foreach ($statusses as $status){
            echo 'Recieved status: package = ' . $package->package_id . ', status = ' . $status .".\n";
            $sta=  PackageStatus::findOne(['status' => (string)$status->Estado, 'package_id' => $package->package_id]);

            if (empty($sta)){

                $sta= new PackageStatus(['status' => (string)$status->Estado, 'description' => (string)$status->Motivo, 'package_id' => $package->package_id]);

                if (!$sta->save()){
                    echo print_r($sta->getErrors(), 1);
                    return false;
                }

                if ($sta->status === 'Entregado'){
                    $package->updateAttributes(['status'=> 'delivered']);
                }
            }
        }

        return true;
    }
}