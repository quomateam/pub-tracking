<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 24/08/18
 * Time: 17:32
 */

namespace quoma\tracking\platforms\oca\components\ocaquotewidget;


use quoma\tracking\models\Package;
use quoma\tracking\platforms\oca\Oca;
use quoma\tracking\TrackingModule;
use yii\bootstrap\Widget;

class OcaQuoteWidget extends Widget
{

    public $model_class;
    public $model_id;
    public $cart= false;
    public $submit_btn;
    public $next_step;
    public $website_id;

    public function run()
    {

        $package= $this->getPackage();

        $localSales= $package->site->getBranches($this->website_id);

        $select_branch= $package->site->select_local_sale;

        $branchs = $this->getBranchs($package);

        return $this->render('view', [
            'package' => $package,
            'submit_btn' => $this->submit_btn,
            'branchs' => $branchs,
            'next_step' => $this->next_step,
            'local_sales' => $localSales,
            'select_branch' => $select_branch
        ]);
    }

    private function getPackage(){
        $package= Package::findOne(['model_class' => $this->model_class, 'model_id' => $this->model_id]);


        return $package;
    }

    private function getBranchs($package){
        $platform= $package->site->platform;

        if ($platform instanceof Oca){
            return $platform->getCentrosImposicion();
        }

        return [];
    }
}