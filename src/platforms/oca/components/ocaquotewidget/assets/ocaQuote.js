
var Oca = new function(){

    this.quoteUrl;
    this.saveAddressUrl;
    this.formUrl
    this.submitBtn;
    this.nextStep;


    this.init= function(quoteUrl, save_address_url, submit_btn, form_url, next_step, select_local, external_branch){
        Oca.quoteUrl= quoteUrl;
        Oca.saveAddressUrl= save_address_url;
        Oca.submitBtn= submit_btn;
        Oca.formUrl= form_url;
        Oca.nextStep= next_step;


        $('.oca-quote').on('click', '#home-btn', function(){
            Oca.quoteHome();
        });

        $('.oca-quote').on('click', '#branch-btn', function(){
            Oca.quoteBranch();
        });

        $('.oca-quote').on('click', '#change-opt', function(e){
            e.preventDefault()
            Oca.getForm();
        });

        $('input[name=shipping-option]').on('change', function(e){
            console.log($(this).val());
            if($(this).val() == "1" ){
                $('.quote-options').show(550);
                $('#branch').fadeOut(250);
                $('#local').fadeOut(250);
                $('#destinatary').fadeIn(550);
                $('.address-form').fadeOut(200);
                console.log(Oca.submitBtn);
                $(Oca.submitBtn).prop('disabled', 'disabled')
                $(Oca.submitBtn).addClass('disabled');
            }else if($(this).val() == "2"){
                $('.quote-options').show(550);
                $('#destinatary').fadeOut(250);
                $('#local').fadeOut(250);
                $('#branch').fadeIn(550);
                $('.address-form').fadeOut(200);
                console.log(Oca.submitBtn);
                $(Oca.submitBtn).prop('disabled', 'disabled')
                $(Oca.submitBtn).addClass('disabled');
            }else{
                //$('.quote-options').fadeOut(550);
                $('#local').fadeIn(250);
                $('#branch').fadeOut(250);
                $('#destinatary').fadeOut(250);
                $('.address-form').fadeOut(200);

                if (select_local === 0){
                    Oca.disableShipping();
                }

                //Oca.disableShipping();
                //$(Oca.submitBtn).removeAttr('disabled');
                //$(Oca.submitBtn).removeClass('disabled');
            }
        });

        $(document).on('change','#local_id',function (e) {
            Oca.disableShipping();
        });
        
        console.log(external_branch);

        if($('#radio-home').is(':checked')){
            $('.quote-options').show(550);
            $('#branch').fadeOut(250);
            $('#local').fadeOut(250);
            $('#destinatary').fadeIn(550);
            $('.address-form').fadeOut(200);
            console.log(Oca.submitBtn);
            $(Oca.submitBtn).prop('disabled', 'disabled')
            $(Oca.submitBtn).addClass('disabled');

            if($('#destiny-cp').val() !== "") {
                Oca.quoteHome();
            }


        }else if($('#radio-branch').is(':checked')){
            $('.quote-options').show(550);
            $('#destinatary').fadeOut(250);
            $('#local').fadeOut(250);
            $('#branch').fadeIn(550);
            $('.address-form').fadeOut(200);
            console.log(Oca.submitBtn);
            $(Oca.submitBtn).prop('disabled', 'disabled')
            $(Oca.submitBtn).addClass('disabled');
            $('#idci').val(external_branch);
            $('#idci').change();
            Oca.quoteBranch();
        }else{
            $('.quote-options').show(550);
            $('#local').fadeIn(250);
            $('#branch').fadeOut(250);
            $('#destinatary').fadeOut(250);
            $('.address-form').fadeOut(200);

            $(Oca.submitBtn).prop('disabled', 'disabled')
            $(Oca.submitBtn).addClass('disabled');
            Oca.disableShipping();
        }

        /**if (select_local === 0 && quoteMethod === 'local_sales'){
            Oca.disableShipping();
        }**/

        $(document).on('click', Oca.submitBtn ,function(){
            Oca.saveAddress();
        })
    }



    this.quoteHome= function(){
        var data= {
            package_id: $('#package_id').val(),
            Package: {
                cp: $('#destiny-cp').val(),
                shipping_type: "door_to_door"
            },

        }

        $.ajax({
            url: Oca.quoteUrl,
            data: data,
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
              $('.quote-options').addClass('loading');
            },
            success: function(response){
                if (response.status === 'success'){

                    $('#shipping_price').html(response.data.price);
                    $('#delivery_date').html(response.data.delivery);

                    var event= new CustomEvent('quote', {detail: {price: response.data.price.replace('$', '')}});

                    document.dispatchEvent(event);
                    //$('.quote-options').fadeOut(500);
                    $('.quote-options').removeClass('loading');
                    $('.address-form').html(response.form).fadeIn(500);
                    $(Oca.submitBtn).removeAttr('disabled');
                    $(Oca.submitBtn).removeClass('disable');
                }else{
                    $('.address-form').html('<div class="alert alert-warning">Hubo un error de comunicación con el Servicio de Oca. Por favor seleccione otra opción de envío o intentelo más tarde</div>').fadeIn(250);
                }

            },
            error: function (error) {
                console.log(error);
            }
        })
    }

    this.quoteBranch= function(){
        var selected=  $('#idci').select2('data')[0].id.split('-');
        var idci= selected[0];
        var cp= selected[1];
        var description= $('#idci').select2('data')[0].text;
        var data= {
            package_id: $('#package_id').val(),
            Package: {
                branch_external_id: idci,
                shipping_type: "door_to_branch",
                street: description,
                cp: cp,
            }
        }

        $.ajax({
            url: Oca.quoteUrl,
            data: data,
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('.quote-options').addClass('loading');
            },
            success: function(response){
                $('#shipping_price').html(response.data.price);
                $('#delivery_date').html(response.data.delivery);

                var event= new CustomEvent('quote', {detail: {price: response.data.price.replace('$', '')}});

                document.dispatchEvent(event);

                //$('.quote-options').fadeOut(500);
                $('.quote-options').removeClass('loading');
                $('.address-form').html(response.form).fadeIn(500);
                $(Oca.submitBtn).removeAttr('disabled');
                $(Oca.submitBtn).removeClass('disable');

            },
            error: function (error) {
                console.log(error);
            }
        })
    }

    this.disableShipping= function(){
        var data= {
            package_id: $('#package_id').val(),
            Package: {
                branch_external_id: $('#local_id').val(),
                shipping_type: "local_sales",
                street: '',
                cp: '',
                price: 0,
                //idci:$('#local_id').val()
            }
        };

        $.ajax({
            url: Oca.quoteUrl,
            data: data,
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('.quote-options').addClass('loading');
            },
            success: function(response){
                $('#shipping_price').html(response.data.price);
                $('#delivery_date').html(response.data.delivery);

                var event= new CustomEvent('quote', {detail: {price: response.data.price.replace('$', '')}});

                document.dispatchEvent(event);

                //$('.quote-options').fadeOut(500);
                $('.quote-options').removeClass('loading');
                $('.address-form').html(response.form).fadeIn(250);
                $('.quote-options').removeClass('loading');
                $(Oca.submitBtn).removeAttr('disabled');
                $(Oca.submitBtn).removeClass('disable');


            },
            error: function (error) {
                console.log(error);
            }
        })
    }

    this.getForm= function () {
        $.ajax({
           url: Oca.formUrl,
           data: $.param({package_id: $('#package_id').val()}),
           dataType: 'json',
           success: function (response) {
               if (response.status === 'success'){
                   $('.quote-options').html(response.form);
               }else{
                   console.log(response.error);
               }

           }
        });
    }

    this.saveAddress= function (){
        $.ajax({
            url: Oca.saveAddressUrl,
            data: $('#address_form').serializeArray(),
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('.address-form').addClass('loading');
            },
            success: function (response) {
                if (response.status === 'success'){
                    window.location.replace(Oca.nextStep);
                }else{
                    $('.address-form').removeClass('loading');
                    $('.messages').empty();
                    $('.messages').append('<div class="alert alert-danger">Debe Completar todos los campos</div>');
                    $('#address_form').yiiActiveForm('updateMessages', response.error, true);
                }
            },

        })
    }
}
