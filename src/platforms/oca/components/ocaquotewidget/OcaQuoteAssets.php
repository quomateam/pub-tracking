<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 28/08/18
 * Time: 15:12
 */

namespace quoma\tracking\platforms\oca\components\ocaquotewidget;


use yii\web\AssetBundle;

class OcaQuoteAssets extends AssetBundle
{
    public $sourcePath = '@vendor/quoma/tracking-module/src/platforms/oca/components/ocaquotewidget/assets';

    public $js= [
        'ocaQuote.js'
    ];

    public $depends= [
      'quoma\core\assets\bootbox\BootboxAsset'
    ];
}