<?php

use quoma\tracking\models\Package;
use quoma\tracking\TrackingModule;

\quoma\tracking\platforms\oca\components\ocaquotewidget\OcaQuoteAssets::register($this);

?>

<div class="oca-quote">
    <div class="row oca-quote-content">

        <div class="col-xs-12 shipping-title-container">
            <h3 class="shipping-title"><?php echo Yii::t('app','Shipping Options')?></h3>
        </div>

        <div class="col-xs-12 col-md-4 shipping-options">
            <h5 class="font-bold">
                Seleccione una opción de envío
            </h5>
            <ul>
                <li>
                    <?php echo \yii\helpers\Html::radio('shipping-option',
                    $package->shipping_type === Package::LOCAL_SALES_SHIPPING_TYPE || $package->shipping_type === null, ['value' => 0, 'id' => 'radio-local'])?>
                    <?php echo Yii::t('app','Collect in local shop')?>
                </li>
                <li>
                    <?php echo \yii\helpers\Html::radio('shipping-option',
                     $package->shipping_type === Package::DOOR_TO_BRANCH_SHIPPING_TYPE, ['value' => 2, 'id' => 'radio-branch'])?>
                    <?php echo Yii::t('app','Send to the nearest Oca branch') ?>
                </li>
                <li>
                    <?php echo \yii\helpers\Html::radio('shipping-option',
                        $package->shipping_type === Package::DOOR_TO_DOOR_SHIPPING_TYPE, ['value' => 1, 'id' => 'radio-home'])?>
                    <?php echo Yii::t('app','Send to my address')?>
                </li>
            </ul>
        </div>

        <div class="col-xs-12 col-md-8 shipping-data-change">
            <?php //echo \yii\bootstrap\Html::a('<span class="glyphicon glyphicon-refresh"></span> Cambiar Opciones de Envío' , '#', ['class' => 'pull-right','id' => 'change-opt'])?>
        </div>

        <div class="col-xs-12 col-md-8 shipping-data">

            <div class="shipping-quote-options quote-options" style="display: none">
                <?php echo $this->renderFile('@vendor/quoma/tracking-module/src/views/package/quote_form.php', ['package' => $package, 'branchs' => $branchs, 'local_sales' => $local_sales, 'select_branch'=> $select_branch])?>
                <input type="hidden" id="has-quote" value="0">
            </div>

           
        </div>


    </div>
    <div class="shipping-price">
        <ul>
            <li>
                <h4>
                   <?php echo Yii::t('app','Shipping Price')?>:
                    <span id="shipping_price"><?php echo $package->price ? Yii::$app->formatter->asCurrency($package->price) : '$0,00'?></span>
                </h4>
            </li>
            <li>
                <h5>
                    <?php echo Yii::t('app', 'Delivery Date')?>:
                    <span id="delivery_date"><?php echo $package->delivery_timestamp ? Yii::$app->formatter->asDate($package->delivery_timestamp, 'dd-MM-yyyy'): 'No Aplica'?></span>
                </h5>
                <input type="hidden" id="package_id" value="<?php echo $package->package_id?>">
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-xs-12 shipping-address-info">
            <div class="shipping-address-info-form address-form" style="display: none">

            </div>
        </div>
    </div>

</div>



<?php $this->registerJs('Oca.init("'.
    \yii\helpers\Url::to(['/tracking/package/quote-package']). '", "'.
    \yii\helpers\Url::to(['/tracking/package/save-address', 'package_id' => $package->package_id]).'", "'.
    $submit_btn .'", "'. \yii\helpers\Url::to(['/tracking/package/get-oca-quote-form', 'package_id' => $package->package_id]).'", "'.
    $next_step.'",'. $select_branch. ',"'. $package->branch_external_id.'-'.$package->cp
.'")')?>

