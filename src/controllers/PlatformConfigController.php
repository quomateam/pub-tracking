<?php

namespace quoma\tracking\controllers;

use quoma\tracking\components\Controller;
use quoma\tracking\models\Site;
use quoma\tracking\TrackingModule;
use Yii;
use quoma\tracking\models\PlatformConfig;
use quoma\tracking\models\search\PlatformConfigSearch;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlarformConfigController implements the CRUD actions for PlatformConfig model.
 */
class PlatformConfigController extends Controller
{
    //public $layout = '//main';

    /**
     * @param $id // id del sitio en el modulo tracking
     * @param null $site_id id del sitio de base web
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionConfigSite($id, $site_id= null){

        if (TrackingModule::getInstance()->multisite){
            if (!empty($site_id)){
                $this->setWebsite($site_id);
            }else{
                $this->layout= '//main';
            }

        }

        $model= new PlatformConfig();

        $site= Site::findOne($id);

        if (empty($site)){
            throw new BadRequestHttpException('Site Not Found');
        }

        $model->site_id= $id;

        if ($model->load(Yii::$app->request->post()) && $model->saveSiteConfig()){
            Yii::$app->session->addFlash('success', Yii::t('app','Site has been saved succesfull'));
            return $this->redirect(['/tracking/site/view', 'id' => $id, 'site_id' => $site_id]);
        }

        $params= $site->trackingPlatform->getConfigParams();

        return $this->render('config', ['site' => $site, 'params' => $params, 'model' => $model]);
    }

    /**
     * Finds the PlatformConfig model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PlatformConfig the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlatformConfig::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
