<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 09/10/18
 * Time: 11:49
 */

namespace quoma\tracking\controllers;


use quoma\tracking\models\Package;
use yii\console\Controller;

class TrackingController extends Controller
{
    public function actionPackageStatus(){
        echo "Package Status started...\n";
        $packages= Package::find()->andWhere(['status' => 'in_process'])->all();

        foreach ($packages as $package){
            $platform= $package->site->getPlatform();

            $platform->updateStatus($package);
        }
        echo "Done.\n";
    }
}