<?php

namespace quoma\tracking\controllers;

use quoma\tracking\components\Controller;
use quoma\tracking\models\TrackingPlatform;
use quoma\tracking\TrackingModule;
use Yii;
use quoma\tracking\models\Site;
use quoma\tracking\models\search\SiteSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * SiteController implements the CRUD actions for Site model.
 */
class SiteController extends Controller
{
    //public $layout= '//main';
    /**
     * Lists all Site models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout= '//main';

        $searchModel = new SiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $platforms= ArrayHelper::map(TrackingPlatform::find()->andWhere(['status' => 'enabled'])->all(), 'tracking_platform_id', 'name');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'platforms' => $platforms,
        ]);
    }

    /**
     * Displays a single Site model.
     * @param integer $id // id del sitio en modulo tracking
     * @param integer $site_id id del sitio de base web, si corresponde
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $site_id= null)
    {
        if (TrackingModule::getInstance()->multisite){
            if (!empty($site_id)){
                $this->setWebsite($site_id);
            }else{
                $this->layout= '//main';
            }

        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'site_id' => $site_id
        ]);
    }

    /**
     * Creates a new Site model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='//main';
        $model = new Site();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (TrackingModule::getInstance()->multisite){
                return $this->redirect(['/tracking/site/index']);
            }
            return $this->redirect(['/tracking/site/view', 'id' => $model->site_id]);
        }

        $platforms= ArrayHelper::map(TrackingPlatform::find()->andWhere(['status' => 'enabled'])->all(), 'tracking_platform_id', 'name');

        /**$website= null;
        if (TrackingModule::getInstance()->multisite){
            $website= $this->getWebsite()->website_id;
        }**/

        //$branches= $model->getBranches($website);

        return $this->render('create', [
            'model' => $model,
            'platforms' => $platforms,
            //'branches' => $branches
        ]);
    }

    /**
     * Updates an existing Site model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout= '//main';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (TrackingModule::getInstance()->multisite){
                return $this->redirect(['/tracking/site/index']);
            }
            return $this->redirect(['/tracking/platform-config/config-site', 'site_id' => $model->site_id]);
        }

        $platforms= ArrayHelper::map(TrackingPlatform::find()->andWhere(['status' => 'enabled'])->all(), 'tracking_platform_id', 'name');


        return $this->render('update', [
            'model' => $model,
            'platforms' => $platforms

        ]);
    }

    /**
     * Deletes an existing Site model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Site model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Site the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Site::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
