<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\tracking\models\Site */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => TrackingModule::t('Sites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> '.Yii::t('app','Update'), ['update', 'id' => $model->site_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> '.Yii::t('app','Delete'), ['delete', 'id' => $model->site_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="glyphicon glyphicon-cog"></span> '.Yii::t('app','Config Tracking Platform'), ['/tracking/platform-config/config-site', 'id' => $model->site_id, 'site_id' => $site_id], [
            'class' => 'btn btn-info',
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'status',
                'value' => function ($model){
                    return Yii::t('app', ucfirst($model->status));
                }
            ],
            'site_token',
            [
                'attribute' => 'trackingPlatform',
                'value' => function ($model){
                    return $model->trackingPlatform->name;
                }
            ],
        ],
    ]) ?>

</div>
