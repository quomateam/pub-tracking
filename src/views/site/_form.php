<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model quoma\tracking\models\Site */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="site-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'enabled' => 'Enabled', 'disabled' => 'Disabled', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'site_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tracking_platform_id')->widget(\kartik\select2\Select2::className(), [
        'data' => $platforms,
        'options' => ['placeholder' => TrackingModule::t('Select a platform')]
    ]) ?>

    <?php echo $form->field($model,'select_local_sale')->checkbox(['checked' => ($model->isNewRecord || $model->select_local_sale === true) ? 'checked' : null])?>


    <div class="form-group">
        <?= Html::submitButton(TrackingModule::t('Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
