<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\tracking\models\search\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Sites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app','Create Site'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'status',
                'value' => function ($model){
                    return TrackingModule::t(ucfirst($model->status));
                },
                'filter' => [
                    'enabled' => Yii::t('app','Enabled'),
                    'disabled' => Yii::t('app','Disabled')
                ]
            ],
            'site_token',
            [
                'attribute' => 'tracking_platform_id',
                'value' => function ($model){
                    return $model->trackingPlatform->name;
                },
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'SiteSearch[tracking_platform_id]',
                    'data' => $platforms,
                    'value' => $searchModel->tracking_platform_id,
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ])
            ],

            ['class' => 'quoma\core\grid\ActionColumn'],
        ],
    ]); ?>
</div>
