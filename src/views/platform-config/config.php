<?php
use quoma\tracking\TrackingModule;
use quoma\tracking\models\PlatformConfig;

$this->title= Yii::t('app','Tracking Platform Config') . ' - '. $site->name;
$this->params['breadcrumbs'][]= ['label' => Yii::t('app','Sites'), 'url' => ['/tracking/site/index']];
$this->params['breadcrumbs'][]= ['label' => $site->name, 'url' => ['/tracking/site/view', 'id' => $site->site_id]];
$this->params['breadcrumbs'][]= ['label' => Yii::t('app','Tracking Platform Config')];
?>

<div class="config-site">

    <h1><?php echo $this->title?></h1>

    <hr>

    <?php $form= \yii\bootstrap\ActiveForm::begin()?>

        <?php
            foreach ($params as $param){
                if ($param['type'] === 'text'){
                    echo $form->field($model, 'site_config['.$param['attribute'].']')
                        ->textInput(['value' => PlatformConfig::getValue($param['attribute'], $site->site_id)])->label($param['label']);
                }elseif($param['type'] === 'drop'){
                    echo $form->field($model, 'site_config['.$param['attribute'].']')
                        ->dropDownList($param['options'], ['value' => PlatformConfig::getValue($param['attribute'], $site->site_id), 'prompt' => TrackingModule::t('Select an option')])->label($param['label']);

                }else{
                    echo $form->field($model, 'site_config['.$param['attribute'].']')
                        ->checkbox(['checked' => PlatformConfig::getValue($param['attribute'], $site->site_id) == 1 ? 'checked' : null])->label($param['label']);
                }
            }
        ?>

    <?php echo \yii\bootstrap\Html::submitButton(Yii::t('app','Save'), ['class' => 'btn btn-success'])?>
    <?php \yii\bootstrap\ActiveForm::end()?>


</div>
