<?php
use quoma\tracking\TrackingModule;

switch($status){
    case \quoma\tracking\models\Package::TYPE_PENDING:
        $this->title = Yii::t('app','Pending Packages');
        break;
    case \quoma\tracking\models\Package::TYPE_CONFIRM:
        $this->title = Yii::t('app','Confirm Packages');
        break;
    case \quoma\tracking\models\Package::TYPE_IN_PROCESS:
        $this->title = Yii::t('app','Packages in Curse');
        break;
    case \quoma\tracking\models\Package::TYPE_DELIVERED:
        $this->title = Yii::t('app','Delivered Packages');
        break;
    case \quoma\tracking\models\Package::TYPE_CANCELED:
        $this->title = Yii::t('app','Canceled Packages');
        break;

}

$this->params['breadcrumbs'][]= $this->title;

?>

<div class="shippings">

    <h1><?php echo $this->title?></h1>

    <hr>

    <?php

    $columns= [
        'customerFullName',
        [
            'attribute' => 'fullAddress',
            'value' => function ($model){
                if ($model->shipping_type === \quoma\tracking\models\Package::LOCAL_SALES_SHIPPING_TYPE){
                    return $model->localSales;
                }

                return $model->fullAddress;

            }
        ],
        'cp',
        'external_id',
        [
            'class' => 'quoma\core\grid\ActionColumn',
            'buttons' => [
                'view' => function($url, $model) use($site_id){
                    return \yii\helpers\Html::a('<span class="glyphicon glyphicon-eye-open">', ['/tracking/package/view', 'package_id' => $model->package_id, 'site_id' => $site_id], ['class' => 'btn btn-default']);
                },
                'tags' => function($url, $model){
                    if ($model->status === 'confirm'){
                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-tags">', ['/tracking/package/package-tags', 'package_id' => $model->package_id], ['class' => 'btn btn-warning', 'target' => '_blank']);
                    }
                    return null;
                },
                'cancel' => function ($url, $model) use($site_id){
                    if ($model->status === 'confirm' || $model->status === 'pending'){
                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-remove">', ['/tracking/package/cancel-package', 'package_id' => $model->package_id, 'site_id' => $site_id], ['class' => 'btn btn-danger']);
                    }

                    return null;
                },
                'send' => function ($url, $model) use($site_id){
                    if ($model->status === 'pending'){
                        return \yii\helpers\Html::a('<span class="glyphicon glyphicon-send">', ['/tracking/package/send-package', 'package_id' => $model->package_id, 'site_id' => $site_id], ['class' => 'btn btn-success']);
                    }
                    return null;
                }
            ],
            'template' => '{view} {tags} {cancel}'
        ]


    ];



    echo \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $search,
        'columns' => $columns
    ])?>

</div>
