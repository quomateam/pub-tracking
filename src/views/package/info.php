<?php
use quoma\tracking\models\Package;
use quoma\tracking\TrackingModule;
?>

<div class="address-info">
    <?php if ($package->shipping_type === Package:: DOOR_TO_DOOR_SHIPPING_TYPE):?>
     <div class="row">
         <div class="col-xs-12 col-md-6 address-info__first-block">
            <h5>
                Persona que recibe el envío <?php TrackingModule::t('Person who receives')?>
            </h5>
            <h4>
                <?php echo $package->customer_name . ' '. $package->customer_lastname?>
            </h4>
         </div>
         <div class="col-xs-12 col-md-6 address-info__second-block">
            <h5>Dirección de Envío <?php TrackingModule::t('Delivery Address')?></h5>
            <ul>
                <li>
                    Calle <?php TrackingModule::t('Street')?>: <?php echo $package->street?>
                </li>
                <li>
                    Número <?php TrackingModule::t('Number')?>: <?php echo $package->number?>
                </li>
                <li>
                    Piso <?php TrackingModule::t('Floor')?>: <?php echo $package->floor?>
                </li>
                <li>
                    Casa/Depto <?php TrackingModule::t('House')?>: <?php echo $package->house?>
                </li>
                <li>
                    Localidad <?php TrackingModule::t('Locality')?>: <?php echo $package->locality?>
                </li>
                <li>
                    Provincia <?php TrackingModule::t('Province')?>: <?php echo $package->province?>
                </li>
            </ul>             
         </div>
         <div class="col-xs-12 col-md-6">
            <?php elseif($package->shipping_type === Package::DOOR_TO_BRANCH_SHIPPING_TYPE):?>
                <h5>
                    Persona quien recibe el envío
                    <?php TrackingModule::t('Person who receives')?>
                </h5>
                <h4><?php echo $package->customer_name . ' '. $package->customer_lastname?></h4>
                <hr>
                <h5>
                    Por entregar en
                    <?php TrackingModule::t('Delivery in branch')?>
                </h5>
                <h4> <?php echo $package->street?></h4>

            <?php endif;?>             
         </div>
     </div>   


</div>
