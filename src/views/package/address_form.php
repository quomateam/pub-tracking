<?php
use quoma\tracking\TrackingModule;
?>

<div class="address-form" style="margin: 10px">
    <div class="row">
        <div class="col-xs-12 shipping-address-info-form-title">
            <hr>

            <h4><?php echo Yii::t('app','Destinatary Data') ?></h4>
            
        </div>
    </div>

    <?php $form= \yii\bootstrap\ActiveForm::begin(['id' => 'address_form'])?>
    <div class="messages">

    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?php echo $form->field($package, 'customer_name')->textInput()?>
        </div>
        <div class="col-lg-6 col-md-6">
            <?php echo $form->field($package, 'customer_lastname')->textInput()?>
        </div>
    </div>
    <?php if ($package->shipping_type === quoma\tracking\models\Package::DOOR_TO_DOOR_SHIPPING_TYPE):?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?php
            $package->street= ''; // Para evitar mostrar informacion incorrecta
            echo $form->field($package, 'street')->textInput()
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <?php echo $form->field($package, 'number')->textInput()?>
        </div>
        <div class="col-lg-4 col-md-4">
            <?php echo $form->field($package, 'floor')->textInput()?>
        </div>
        <div class="col-lg-4 col-md-4">
            <?php echo $form->field($package, 'house')->textInput()?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?php echo $form->field($package, 'locality')->textInput()?>
        </div>
        <div class="col-lg-6 col-md-6">
            <?php echo $form->field($package, 'province')->textInput()?>
        </div>
    </div>
    <?php elseif ($package->shipping_type === quoma\tracking\models\Package::DOOR_TO_BRANCH_SHIPPING_TYPE):?>
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <?php echo $form->field($package, 'cp')->textInput()?>
            </div>
            <div class="col-lg-4 col-md-4">
                <?php echo $form->field($package, 'locality')->textInput()?>
            </div>
            <div class="col-lg-4 col-md-4">
                <?php echo $form->field($package, 'province')->textInput()?>
            </div>
        </div>

    <?php endif;?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?php echo $form->field($package, 'phone')->textInput()?>
        </div>
        <div class="col-lg-6 col-md-6">
            <?php echo $form->field($package, 'email')->textInput()?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 text-center">
            <?php //echo \yii\helpers\Html::button('Guardar', ['class' => 'btn btn-primary', 'id' => 'save-addr-btn'])?>
        </div>
    </div>

    <?php \yii\bootstrap\ActiveForm::end()?>

</div>
