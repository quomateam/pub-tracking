<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 20/09/18
 * Time: 16:17
 */
$this->title= 'Paquete: ' . $package->package_id;

switch($package->status){
    case \quoma\tracking\models\Package::TYPE_PENDING:
        $index = Yii::t('app','Pending Packages');
        break;
    case \quoma\tracking\models\Package::TYPE_CONFIRM:
        $index = Yii::t('app','Confirm Packages');
        break;
    case \quoma\tracking\models\Package::TYPE_IN_PROCESS:
        $index = Yii::t('app','Packages in Curse');
        break;
    case \quoma\tracking\models\Package::TYPE_DELIVERED:
        $index = Yii::t('app','Delivered Packages');
        break;
    case \quoma\tracking\models\Package::TYPE_CANCELED:
        $index = Yii::t('app','Canceled Packages');
        break;

}

$this->params['breadcrumbs'][]= ['label' => $index, 'url' => ['/tracking/package/index',
    'site_token' => $package->site->site_token, 'site_id' => $site_id, 'status' => $package->status]];
$this->params['breadcrumbs'][]= $this->title;
?>

<div class="package-view">

    <h1><?php echo $this->title?></h1>

    <p>
        <?php
            if ($package->status === 'pending' && $package->shipping_type !== \quoma\tracking\models\Package::LOCAL_SALES_SHIPPING_TYPE){
                echo \yii\helpers\Html::a('<span class="glyphicon glyphicon-send"></span>  '. Yii::t('app','Request Withdrawal'), ['/tracking/package/send-package', 'package_id' => $package->package_id, 'site_id' => $site_id], ['class' => 'btn btn-success',
                    'data' => [
                        'confirm' => Yii::t('app','Are you sure you wish request Withdrawal this package?'),
                        'method' => 'post',
                    ],
                ]);
                echo ' ';
            }
            if ($package->status === 'confirm' || ($package->status === 'pending' && $package->shipping_type === \quoma\tracking\models\Package::LOCAL_SALES_SHIPPING_TYPE)){
                echo \yii\helpers\Html::a('<span class="glyphicon glyphicon-send"></span>  ' . Yii::t('app','Mark as retired'), ['/tracking/package/in-progress-package', 'package_id' => $package->package_id, 'site_id' => $site_id], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => Yii::t('app','Are you sure you wish Mark as retired this package?'),
                        'method' => 'post',
                    ],
                ]);
                echo ' ';
            }
            if ($package->status === 'in_process'){
                echo \yii\helpers\Html::a('<span class="glyphicon glyphicon-send"></span>  ' . Yii::t('app','Mark as delivered'), ['/tracking/package/delivered-package', 'package_id' => $package->package_id, 'site_id' => $site_id], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => Yii::t('app','Are you sure you wish Mark as Delivered this package?'),
                        'method' => 'post',
                    ],
                ]);
                echo ' ';
            }
            if ($package->status === 'confirm' && $package->shipping_type !== \quoma\tracking\models\Package::LOCAL_SALES_SHIPPING_TYPE){
                echo \yii\helpers\Html::a('<span class="glyphicon glyphicon-tags"></span>  '. Yii::t('app','Print Tags'), ['/tracking/package/package-tags', 'package_id' => $package->package_id, 'site_id' => $site_id], ['class' => 'btn btn-warning', 'target' => '_blank']);
                echo ' ';
            }

            if ($package->status === 'confirm' || $package->status === 'pending' && $package->shipping_type !== \quoma\tracking\models\Package::LOCAL_SALES_SHIPPING_TYPE){
                echo \yii\helpers\Html::a('<span class="glyphicon glyphicon-remove"></span>  ' . Yii::t('app','Cancel Package'), ['/tracking/package/cancel-package', 'package_id' => $package->package_id, 'site_id' => $site_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app','Are you sure you wish cancel this package?'),
                        'method' => 'post',
                    ],
                ]);
                echo ' ';
            }
        ?>
    </p>

    <h3>Detalles del Paquete</h3>
    <?php
        if ($package->shipping_type === \quoma\tracking\models\Package::DOOR_TO_DOOR_SHIPPING_TYPE){

            echo \yii\widgets\DetailView::widget([
                'model' => $package,
                'attributes' => [
                    'customerFullName',
                    'street',
                    'number',
                    'floor',
                    'house',
                    'locality',
                    'province',
                    'cp',
                    'phone',
                    'email',
                    'price',
                    'external_id',
                ]
            ]);
        }elseif ($package->shipping_type === \quoma\tracking\models\Package::DOOR_TO_BRANCH_SHIPPING_TYPE){
            echo \yii\widgets\DetailView::widget([
                'model' => $package,
                'attributes' => [
                    'customerFullName',
                    [
                        'attribute' => 'street',
                        'label' => 'Retira por sucursal'
                    ],
                    'phone',
                    'email',
                    'price',
                    'external_id',
                ]
            ]);
        }else{
            echo \yii\widgets\DetailView::widget([
                'model' => $package,
                'attributes' => [
                    'customerFullName',
                    [
                        'attribute' => 'localSales',
                        'label' => 'Retira por sucursal'
                    ],
                    'phone',
                    'email',
                    'price'
                ]
            ]);
        }
    ?>


    <h3>Productos</h3>

    <?php echo \yii\grid\GridView::widget([
        'dataProvider' => $products,
        'columns' => [
            [
                'label' => Yii::t('app','Product'),
                'value' => function($model){
                    return $model->product->name;
                }
            ],
            [
                'attribute' => 'qty',
            ],
        ]
    ])?>

    <h3>Seguimiento del Paquete</h3>

    <?php echo \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $package->getPackageStatusses()]),
        'columns' => [
            'status',
            'description',
            'timestamp:datetime'
        ]
    ])?>

</div>


