<?php
use quoma\tracking\TrackingModule;
?>

<div class="shipping-tabs-content">
    <div id="local" style="display: none">
        <br>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label><?php echo Yii::t('app','Select a Local Sale')?></label>
                    <?php echo \kartik\select2\Select2::widget([
                        'name' => 'local-sale',
                        'data' => $local_sales,
                        'value' => array_keys($local_sales)[0],
                        'options' => [
                            'placeholder' => Yii::t('app','Select a Local Sale'),
                            'id' => 'local_id'

                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])?>
                </div>
            </div>

        </div>

    </div>
    <div id="destinatary" style="display: none">
        <br>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Ingresa tú Código Postal</label>
                    <?php echo \yii\bootstrap\Html::textInput('destiny-cp', $package->cp, ['class' => 'form-control', 'id' => 'destiny-cp', 'placeholder' => 'CP'])?>
                </div>
            </div>

            <div class="col-xs-12 col-md-2">
                <br>
                <button class="btn btn-primary" id="home-btn"><?php echo Yii::t('app','Quote Package')?></button>
            </div>
        </div>

    </div>
    <div id="branch" style="display: none">
        <?php if (count($branchs) < 1):?>
            <div class="alert alert-warning">
                <h4>El servicio de envío a una sucursal de Oca no se encuentra disponible en este momento. Seleccione otra opción de envío </h4>
            </div>
        <?php endif;?>
        <br>
        <div class="row">
            <div class="col-xs-12 col-md-10">

                <label for="idci">Selecciona la sucursal de Oca más cercana a tu domicilio</label>
                <?php echo \kartik\select2\Select2::widget([
                    'name' => 'idci',
                    'data' => $branchs,
                    'value' => $package->branch_external_id,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'options' => [
                        'placeholder' => 'Buscá la sucursal de Oca más cercana',
                        'id' => 'idci',
                        'disabled' => count($branchs) < 1 ? 'disabled': null,
                    ]
                ])?>
            </div>
            <div class="col-xs-12 col-md-2">
                <br>
                <button class="btn btn-primary" id="branch-btn" <?php echo count($branchs) < 1 ? 'disabled': null ?> ><?php echo Yii::t('app','Quote Package')?></button>
            </div>
        </div>

    </div>
</div>
