<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\tracking\models\search\TrackingPlatformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Tracking Platforms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-platform-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> '. Yii::t('app','Create Tracking Platform'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'class_name',
            [
                'attribute' => 'status',
                'value' => function ($model){
                    return Yii::t('app', ucfirst($model->status));
                },
                'filter' => [
                    'enabled' => Yii::t('app','Enabled'),
                    'disabled' => Yii::t('app','Disabled')]
            ],

            ['class' => 'quoma\core\grid\ActionColumn'],
        ],
    ]); ?>
</div>
