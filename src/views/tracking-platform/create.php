<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tracking\models\TrackingPlatform */

$this->title = Yii::t('app','Create Tracking Platform');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Tracking Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-platform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
