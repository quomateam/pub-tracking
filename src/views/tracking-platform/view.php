<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\tracking\models\TrackingPlatform */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => TrackingModule::t('Tracking Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tracking-platform-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> '. Yii::t('app','Update'), ['update', 'id' => $model->tracking_platform_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> '. Yii::t('app','Delete'), ['delete', 'id' => $model->tracking_platform_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app','Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'class_name',
            [
                'attribute' => 'status',
                'value' => function ($model){
                    return Yii::t('app', ucfirst($model->status));
                }
            ],
        ],
    ]) ?>

</div>
