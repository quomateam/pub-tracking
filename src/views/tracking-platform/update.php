<?php

use quoma\tracking\TrackingModule;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tracking\models\TrackingPlatform */

$this->title = Yii::t('app','Update Tracking Platform: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Tracking Platforms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->tracking_platform_id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tracking-platform-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
