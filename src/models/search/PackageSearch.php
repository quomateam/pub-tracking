<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 04/09/18
 * Time: 17:48
 */

namespace quoma\tracking\models\search;


use quoma\tracking\models\Package;
use yii\data\ActiveDataProvider;

class PackageSearch extends Package
{

    public function search($params){
        $query= Package::find();

        $this->load($params);

        $query->andFilterWhere([
            'package_id' => $this->package_id,
            'site_id' => $this->site_id,
            'status' => $this->status,
            'customer_name' => $this->customer_name,
            'customer_lastname' => $this->customer_lastname
        ]);

        //Los package en draft corresponden a paquetes que no se enviaron o los que estan en proceso
        $query->andWhere(['<>', 'status', 'draft']);

        $query->orderBy(['package_id' => SORT_DESC]);

        $dataProvider= new ActiveDataProvider(['query' => $query]);

        return $dataProvider;

    }

}