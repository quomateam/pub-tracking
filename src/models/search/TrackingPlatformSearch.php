<?php

namespace quoma\tracking\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use quoma\tracking\models\TrackingPlatform;

/**
 * TrackingPlatformSearch represents the model behind the search form of `common\modules\tracking\models\TrackingPlatform`.
 */
class TrackingPlatformSearch extends TrackingPlatform
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tracking_platform_id'], 'integer'],
            [['name', 'class_name', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrackingPlatform::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tracking_platform_id' => $this->tracking_platform_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'class_name', $this->class_name])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
