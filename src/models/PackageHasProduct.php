<?php

namespace quoma\tracking\models;

use quoma\tracking\TrackingModule;
use Yii;

/**
 * This is the model class for table "package_has_product".
 *
 * @property int $package_has_product_id
 * @property string $product_class
 * @property int $product_id
 * @property int $package_id
 * @property int $qty
 *
 * @property Package $package
 */
class PackageHasProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_has_product';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_tracking');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_class', 'product_id', 'package_id'], 'required'],
            [['product_id', 'package_id', 'qty'], 'integer'],
            [['qty'], 'safe'],
            [['product_class'], 'string', 'max' => 255],
            [['package_id'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['package_id' => 'package_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'package_has_product_id' => Yii::t('app',  'Package Has Product ID'),
            'product_class' => Yii::t('app',  'Product Class'),
            'product_id' => Yii::t('app',  'Product ID'),
            'package_id' => Yii::t('app',  'Package ID'),
            'product' => Yii::t('app','Product'),
            'qty' => Yii::t('app','Count')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['package_id' => 'package_id']);
    }

    public function getProduct(){
        $class= $this->product_class;
        //var_dump($this); die();

        $pk= $class::primaryKey();

        return $this->hasOne($class, [$pk[0] => 'product_id']);
    }

    public function getVolumen(){
        $product = $this->product;

        if ($product){
            $volumen= $product->width * $product->height * $product->large; // en mm3
            return ($volumen / 1000000000) * $this->qty; // transformamos a m3 y multiplicamos por la cantidad de unidades del producto
        }

        return  0;
    }
}
