<?php

namespace quoma\tracking\models;

use Yii;

/**
 * This is the model class for table "platform_config".
 *
 * @property int $platform_config_id
 * @property string $attribute
 * @property string $value
 * @property int $site_id
 *
 * @property Site $site
 */
class PlatformConfig extends \yii\db\ActiveRecord
{

    public $site_config= [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'platform_config';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_tracking');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute', 'site_id'], 'required'],
            [['site_id'], 'integer'],
            [['attribute', 'value'], 'string', 'max' => 45],
            [['site_config'], 'safe'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Site::className(), 'targetAttribute' => ['site_id' => 'site_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'platform_config_id' => Yii::t('app',  'Platform Config ID'),
            'attribute' => Yii::t('app',  'Attribute'),
            'value' => Yii::t('app',  'Value'),
            'site_id' => Yii::t('app',  'Site ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Site::className(), ['site_id' => 'site_id']);
    }

    public static function getValue($attr, $site_id){
        $config= self::find()->andWhere(['site_id' => $site_id, 'attribute' => $attr])->one();

        if (empty($config)){
            return null;
        }


        return $config->value;
    }

    public function saveSiteConfig(){
        $transaction= Yii::$app->db_tracking->beginTransaction();
        foreach ($this->site_config as $attr => $value){
            $config = PlatformConfig::findOne(['attribute' => $attr, 'site_id' => $this->site_id]);

            if ($config){
                $config->updateAttributes(['value' => $value]);
            }else{
                $config= new PlatformConfig([
                    'attribute' => $attr,
                    'value' => $value,
                    'site_id' => $this->site_id
                ]);

                if (!$config->save()){
                    $transaction->rollback();
                    Yii::$app->session->addFlash('error', Yii::t('app','Cant save site configuration'));
                    return false;
                }
            }
        }

        $transaction->commit();
        return true;
    }
}
