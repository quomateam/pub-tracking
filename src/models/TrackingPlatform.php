<?php

namespace quoma\tracking\models;

use quoma\tracking\TrackingModule;
use quoma\core\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "tracking_platform".
 *
 * @property int $tracking_platform_id
 * @property string $name
 * @property string $class_name
 * @property string $status
 *
 * @property Site[] $sites
 */
class TrackingPlatform extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tracking_platform';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_tracking');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'class_name', 'status'], 'required'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 45],
            [['class_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tracking_platform_id' => Yii::t('app',  'Tracking Platform ID'),
            'name' => Yii::t('app',  'Name'),
            'class_name' => Yii::t('app',  'Class Name'),
            'status' => Yii::t('app',  'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(Site::className(), ['tracking_platform_id' => 'tracking_platform_id']);
    }

    public function getConfigParams(){
        $class= $this->class_name;

        $platform= new $class;

        return $platform->getConfigParams();
    }

    public function getDeletable()
    {
        return !Site::find()->andWhere(['tracking_platform_id' => $this->tracking_platform_id])->exists();
    }

}
